#include "mainApp.h"

MainApp::MainApp() {
}

MainApp::~MainApp() {
	destroy();
}

bool MainApp::init(const std::string &title, int w, int h, bool fs) {
	root = new Root("plugins.cfg","");

	LogManager::getSingleton().setLogDetail(LL_BOREME);

	if (!createRenderSystem()) 
		 return false;

	root->initialise(false);
	createRenderWindow(title,w,h,fs);
	WindowEventUtilities::addWindowEventListener(win,this);
	createResources();

	game = new Game();
	game->init(root,win);

	return true;
}

void MainApp::destroy() {
	if (game)
		delete game;
	game = NULL;

	if (root)
		delete root;
	root = NULL;
}

void MainApp::run() {
	unsigned long tLast = 0;

	while (game->isRunning()) {
		unsigned long tNow = root->getTimer()->getMilliseconds();
		unsigned long tDelta = tNow - tLast;
		tLast = tNow;

		WindowEventUtilities::messagePump();

		game->capture();
		game->update(tDelta/1000.0f);

		root->renderOneFrame();
	}
}

bool MainApp::createRenderSystem() {
	RenderSystemList *rSyses = root->getAvailableRenderers();
	RenderSystemList::iterator it = rSyses->begin();
	while (it != rSyses->end()) {
		if ((rSys = *(it++))->getName() == "OpenGL Rendering Subsystem") {
			root->setRenderSystem(rSys);
			break;
		}
	}

	if (root->getRenderSystem() == NULL) {
		std::cerr << "Chyba: Nelze nastavit rendering subsystem\n";
		return false;
	}

	return true;
}

void MainApp::createRenderWindow(const std::string &title, int w, int h, bool fs) {
	NameValuePairList winAttrs;
	winAttrs["title"] = title;
	win = root->createRenderWindow("MainWin",w,h,fs,&winAttrs);
}

void MainApp::createResources() {
	ResourceGroupManager *rgm = ResourceGroupManager::getSingletonPtr();
	rgm->addResourceLocation("data","FileSystem","General");
	rgm->addResourceLocation("data/fonts","FileSystem","General");
	rgm->initialiseAllResourceGroups();
}

void MainApp::windowResized(RenderWindow *rw) {
	unsigned int w,h,d;
	int t,l;
	rw->getMetrics(w,h,d,t,l);
	game->setWinSize(w,h);
}

void MainApp::windowClosed(RenderWindow *rw) {
	game->quit();
}

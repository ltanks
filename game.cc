#include "game.h"
#include "utils.h"

#include <btBulletDynamicsCommon.h>

#define MAX_PROXIES (25*25+1*(51))

class myMotionState : public btMotionState {
	public:
		void getWorldTransform(btTransform &wt) const {
		}

		void setWorldTransform(const btTransform &wt) {
		}
};

Game::Game() {
	running = true;
	menu_visible = false;
}

Game::~Game () {
	if (gui_system)
		delete gui_system;
	gui_system = NULL;

	if (gui_render)
		delete gui_render;
	gui_render = NULL;

	if (input) {
		if (keyboard)
			input->destroyInputObject(keyboard);

		if (mouse)
			input->destroyInputObject(mouse);

		input->destroyInputSystem(input);
	}

	for (int i = col_world->getNumCollisionObjects()-1; i >= 0; i--) {
		btCollisionObject *obj = col_world->getCollisionObjectArray()[i];
		btRigidBody *body = btRigidBody::upcast(obj);

		if (body && body->getMotionState())
			delete body->getMotionState();

		col_world->removeCollisionObject(obj);
		delete obj;
	}

	for (int i = 0; i < col_shapes.size(); i++) {
		btCollisionShape *sh = col_shapes[i];
		delete sh;
	}

	delete col_world;
	delete col_solver;
	delete col_pair_cache;
	delete col_dispatch;
	delete col_conf;
}

void Game::init(Root *root, RenderWindow *win, bool buf) {
	sceneMgr = root->createSceneManager(ST_GENERIC);

	initInput(win, buf);
	initCamera();
	initViewport(win);
	initGUI(win);

	OverlayManager *overlay = OverlayManager::getSingletonPtr();
	overlay->getByName("PanelOverlay")->show();

	cam_pos = static_cast<OverlayElement*>(overlay->getOverlayElement("CamPos"));
	cam_pos->setCaption("cam: "+StringConverter::toString(cam->getPosition()));

	loadLevel("level01");

	player.init(sceneMgr, "player1");
	player.setKeys(OIS::KC_W, OIS::KC_S, OIS::KC_A, OIS::KC_D, OIS::KC_G);
	player.setPos(Vector3(-50+15,0,-50+15));
	player.setDir(Vector3(-1,0,0));
	col_world->addRigidBody(player.getBody());
	col_shapes.push_back(player.getBody()->getCollisionShape());

	pl_pos = static_cast<OverlayElement*>(overlay->getOverlayElement("PlayerPos"));
	pl_pos->setCaption("pos: "+StringConverter::toString(player.getPos()));
	pl_dir = static_cast<OverlayElement*>(overlay->getOverlayElement("PlayerDir"));
	pl_dir->setCaption("dir: "+StringConverter::toString(player.getDir()));
	pl_speed = static_cast<OverlayElement*>(overlay->getOverlayElement("PlayerSpeed"));
	pl_speed->setCaption("speed: "+StringConverter::toString(player.getSpeed()));
}

void Game::initInput(RenderWindow *win, bool buf) {
	size_t handle;
	win->getCustomAttribute("WINDOW",&handle);

	input = OIS::InputManager::createInputSystem(handle);

	keyboard = static_cast<OIS::Keyboard*>(input->createInputObject(OIS::OISKeyboard, buf));
	keyboard->setEventCallback(this);

	mouse = static_cast<OIS::Mouse*>(input->createInputObject(OIS::OISMouse, buf));
	mouse->setEventCallback(this);

	unsigned int w,h,d;
	int l,t;
	win->getMetrics(w,h,d,t,l);
	setMouseArea(w,h);
}

void Game::initCamera() {
	cam = sceneMgr->createCamera("cam1");
	cam->setNearClipDistance(5.0f);
	cam->setFarClipDistance(1000.0f);

	cam->setPosition(Vector3(0,110,44)); // 106.5, 49
	cam->lookAt(Vector3(0,0,0));
}

void Game::initViewport(RenderWindow *win) {
	vp = win->addViewport(cam);
	vp->setBackgroundColour(ColourValue::Black);
	vp->setDimensions(0.0f,0.0f,1.0f,1.0f);
	vp->setSkiesEnabled(false);
	cam->setAspectRatio(Real(vp->getActualWidth())/Real(vp->getActualHeight())); 
}

void Game::initGUI(RenderWindow *win) {
	gui_render = new CEGUI::OgreCEGUIRenderer(win, Ogre::RENDER_QUEUE_OVERLAY, false, 0, sceneMgr);
	gui_system = new CEGUI::System(gui_render);

	CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLook.scheme");
	CEGUI::FontManager::getSingleton().createFont("DejaVuSans-10.font",(CEGUI::utf8*)"General");
	gui_system->setDefaultFont((CEGUI::utf8*)"DejaVuSans-10");

	gui_system->setDefaultMouseCursor("TaharezLook", "MouseTarget");
	CEGUI::MouseCursor::getSingletonPtr()->setPosition(CEGUI::Point(0,0));

//	gui_cursor = CEGUI::MouseCursor::getSingletonPtr();
//	gui_cursor->setImage("TaharezLook", "MouseTarget");
//	gui_cursor->setPosition(CEGUI::Point(0,0));
//	gui_cursor->offsetPosition(CEGUI::Point(-gui_cursor->getImage()->getWidth()/2,-gui_cursor->getImage()->getHeight()/2));

	gui_main_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("main_menu.layout", "main_menu/", "General");
	gui_single_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("single_menu.layout", "single_menu/", "General");
	gui_multi_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("multi_menu.layout", "multi_menu/", "General");
	gui_setup_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("setup_menu.layout", "setup_menu/", "General");
	gui_credits_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("credits_menu.layout", "credits_menu/", "General");
	gui_player_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("player_menu.layout", "player_menu/", "General");
	gui_graphics_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("graphics_menu.layout", "graphics_menu/", "General");
	gui_sound_menu = CEGUI::WindowManager::getSingleton().loadWindowLayout("sound_menu.layout", "sound_menu/", "General");

	CEGUI::Window *btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("main_menu/single_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("main_menu/multi_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("main_menu/setup_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("setup_menu/player_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("setup_menu/graphics_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("setup_menu/sound_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("main_menu/credits_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("main_menu/quit_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));

	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("single_menu/back_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("multi_menu/back_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("setup_menu/back_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("credits_menu/back_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("player_menu/back_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("graphics_menu/back_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));
	btn = static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingletonPtr()->getWindow("sound_menu/back_btn"));
	btn->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&Game::menuOnClick, this));

	gui_main_menu->hide();
	gui_system->setGUISheet(gui_main_menu);
}

bool Game::menuOnClick(const CEGUI::EventArgs& args) {
	const CEGUI::WindowEventArgs &w = static_cast<const CEGUI::WindowEventArgs&>(args);
	CEGUI::String id = w.window->getName();

	if (id == "main_menu/single_btn")
		gui_system->setGUISheet(gui_single_menu);
	if (id == "main_menu/multi_btn")
		gui_system->setGUISheet(gui_multi_menu);
	if (id == "main_menu/setup_btn")
		gui_system->setGUISheet(gui_setup_menu);
	if (id == "main_menu/credits_btn")
		gui_system->setGUISheet(gui_credits_menu);
	if (id == "main_menu/quit_btn")
		quit();

	if (id == "single_menu/back_btn")
		gui_system->setGUISheet(gui_main_menu);

	if (id == "multi_menu/back_btn")
		gui_system->setGUISheet(gui_main_menu);

	if (id == "setup_menu/player_btn")
		gui_system->setGUISheet(gui_player_menu);
	if (id == "setup_menu/graphics_btn")
		gui_system->setGUISheet(gui_graphics_menu);
	if (id == "setup_menu/sound_btn")
		gui_system->setGUISheet(gui_sound_menu);
	if (id == "setup_menu/back_btn")
		gui_system->setGUISheet(gui_main_menu);

	if (id == "player_menu/back_btn")
		gui_system->setGUISheet(gui_setup_menu);

	if (id == "sound_menu/back_btn")
		gui_system->setGUISheet(gui_setup_menu);

	if (id == "graphics_menu/back_btn")
		gui_system->setGUISheet(gui_setup_menu);

	if (id == "credits_menu/back_btn")
		gui_system->setGUISheet(gui_main_menu);

	return true;
}

void Game::setMouseArea(int w, int h) {
	const OIS::MouseState &ms = mouse->getMouseState();
	ms.width = w;
	ms.height = h;
}

void Game::setWinSize(int w, int h) {
	setMouseArea(w,h);
	cam->setAspectRatio(Real(vp->getActualWidth())/Real(vp->getActualHeight())); 
}

void Game::capture() {
	keyboard->capture();
	mouse->capture();
}

void Game::update(float dt) {
	if (keyboard->isKeyDown(OIS::KC_UP))
		cam->moveRelative(Vector3(0,0,-0.1));
	if (keyboard->isKeyDown(OIS::KC_DOWN))
		cam->moveRelative(Vector3(0,0,0.1));
	if (keyboard->isKeyDown(OIS::KC_LEFT))
		cam->moveRelative(Vector3(0,-0.1,0));
	if (keyboard->isKeyDown(OIS::KC_RIGHT))
		cam->moveRelative(Vector3(0,0.1,0));

	cam_pos->setCaption("cam: "+StringConverter::toString(cam->getPosition()));

	if (!menu_visible) {
		if (keyboard->isKeyDown(player.getKeyLeft())) 
			player.turnLeft();
		if (keyboard->isKeyDown(player.getKeyRight())) 
			player.turnRight();
		if (keyboard->isKeyDown(player.getKeyUp())) 
			player.moveForward();
		if (keyboard->isKeyDown(player.getKeyDown())) 
			player.moveBack();

		const OIS::MouseState ms = mouse->getMouseState();
		if (ms.X.rel != 0 || ms.Y.rel != 0) {
			Ray ray = cam->getCameraToViewportRay(ms.X.abs/float(ms.width), ms.Y.abs/float(ms.height));
			std::pair<bool, Real> dist = ray.intersects(Plane(Vector3::UNIT_Y, 0));
			player.aim(ray.getPoint(dist.second));
		}

		player.preupdate(dt);
		col_world->stepSimulation(dt,1);
		player.postupdate(dt);
	}

	pl_pos->setCaption("pos: "+StringConverter::toString(player.getPos()));
	pl_dir->setCaption("dir: "+StringConverter::toString(player.getDir()));
	pl_speed->setCaption("speed: "+StringConverter::toString(player.getSpeed()));
}

void Game::quit() {
	running = false;
}

bool Game::loadLevel(const std::string &name) {
	FILE *fr;
	if ((fr = fopen("data/levels/level01.map", "r")) == NULL) {
		std::cerr << "Chyba: nelze otevrit soubor data/levels/level01.map\n";
		return false;
	}

	int tmp;
	for (int y = 0; y < MAP_SIZE; y++) {
		for (int x = 0; x < MAP_SIZE; x++) {
			tmp = fgetc(fr);
			map[x][y] = tmp - '0';
		}
		fgetc(fr);
	}
	fclose(fr);

	sceneMgr->setAmbientLight(ColourValue(0.25,0.25,0.25));

	Light *l = sceneMgr->createLight("light1");
	l->setPosition(0,200,0);
	//l->setDiffuseColour(ColourValue::White);
	//l->setSpecularColour(ColourValue::White);

	Plane ground(Vector3::UNIT_Y, 0);
	MeshManager::getSingleton().createPlane("ground_mesh",ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,ground,100,100,10,10,true,1,1,1,Vector3::UNIT_Z);
	Entity *ground_ent = sceneMgr->createEntity("ground_ent","ground_mesh");
	ground_ent->setMaterialName("Green");
	ground_ent->setQueryFlags(0);
	sceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ground_ent);

	MeshPtr cube_mesh = createCubeMesh(sceneMgr, "cube_mesh", "Yellow", CUBE_SIZE);
//	Entity *cube_ent;// = sceneMgr->createEntity("cube_ent1", "cube_mesh");

	map_node = sceneMgr->getRootSceneNode()->createChildSceneNode("map_node");
	map_node->setPosition(-50.5,0,-50.5);

	SceneNode *cube;
	Entity *ent;
	for (int y = 0; y < MAP_SIZE; y++)
		for (int x = 0; x < MAP_SIZE; x++) 
			if (map[x][y]) {
				cube = map_node->createChildSceneNode();
				ent = sceneMgr->createEntity("cube_ent_x"+StringConverter::toString(x)+"_y"+StringConverter::toString(y), "cube_mesh");
				ent->setQueryFlags(WALL_QUERY_MASK);
				cube->attachObject(ent);
				cube->setPosition(x*CUBE_SIZE,0,y*CUBE_SIZE);
			}

	initPhysics();
	
	return true;
}

void Game::initPhysics() {
	col_conf = new btDefaultCollisionConfiguration();
	col_dispatch = new btCollisionDispatcher(col_conf);

	btVector3 min_size(-2000, -5, -2000), max_size(2000, 5, 2000);
	col_pair_cache = new btAxisSweep3(min_size, max_size, MAX_PROXIES);

	col_solver = new btSequentialImpulseConstraintSolver;
	col_world = new btDiscreteDynamicsWorld(col_dispatch, col_pair_cache, col_solver, col_conf);
	col_world->setGravity(btVector3(0,0,0));

	btCollisionShape *shape_wall = new btBoxShape(btVector3(2.5,2.5,2.5));  // wall, player (5x5x5)
	col_shapes.push_back(shape_wall);
//	btCollisionShape *shape_bullet = new btBoxShape(btVector3(2.5,2.5,2.5)*0.3);  // bullet 0.3*(5x5x5)
//	col_shapes.push_back(shape_bullet);

	btTransform transform;
//	btDefaultMotionState *ms;
//	btRigidBody::btRigidBodyConstructionInfo rb_info;
//	btRigidBody *body;
	for (int y = 0; y < MAP_SIZE; y++)
		for (int x = 0; x < MAP_SIZE; x++) 
			if (map[x][y]) {
				transform.setIdentity();
				transform.setOrigin(btVector3(-50.5+x*CUBE_SIZE, 0, -50.5+y*CUBE_SIZE));

				btDefaultMotionState *ms = new btDefaultMotionState(transform);
				btRigidBody::btRigidBodyConstructionInfo rb_info(0.0, ms, shape_wall, btVector3(0,0,0));
				btRigidBody *body = new btRigidBody(rb_info);

				col_world->addRigidBody(body);
			}

	//col_world->getDebugDrawer()->setDebugMode(1);
//	GLDebugDrawer *dr = new GLDebugDrawer();
//	dr->setDebugMode(btIDebugDraw::DBG_DrawAabb);
//	col_world->setDebugDrawer(dr);
}

bool Game::keyPressed(const OIS::KeyEvent &e) {
	if (menu_visible) {
		gui_system->injectKeyDown(e.key);
		gui_system->injectChar(e.text);
	}

	return true;
}

bool Game::keyReleased(const OIS::KeyEvent &e) {
	if (menu_visible) {
		gui_system->injectKeyUp(e.key);
	}
	else {
		if (e.key == player.getKeyFire())
			player.fire(col_world);
	}

	switch (e.key) {
		case OIS::KC_Q: quit();
				break;
		case OIS::KC_ESCAPE: menu_visible = !menu_visible;
				     if (menu_visible) {
					gui_system->getGUISheet()->show();
					gui_system->setDefaultMouseCursor("TaharezLook", "MouseArrow");
//					gui_cursor->setImage("TaharezLook", "MouseArrow");
//					gui_cursor->offsetPosition(CEGUI::Point(0, 0));
				     }
				     else {
					gui_system->getGUISheet()->hide();
					gui_system->setDefaultMouseCursor("TaharezLook", "MouseTarget");
//					gui_cursor->setImage("TaharezLook", "MouseTarget");
//					gui_cursor->offsetPosition(CEGUI::Point(-gui_cursor->getImage()->getWidth()/2,-gui_cursor->getImage()->getHeight()/2));
				     }
				     break;
		default: break;
	}

	return true;
}

bool Game::mouseMoved(const OIS::MouseEvent &e) {
	gui_system->injectMouseMove(e.state.X.rel, e.state.Y.rel); // to update crosshair

	return true;
}

bool Game::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	if (menu_visible) {
		gui_system->injectMouseButtonDown(convertButton(id));
	}
	else {
		if (id == OIS::MB_Left)
			player.fire(col_world);
	}

	return true;
}

bool Game::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	if (menu_visible) {
		gui_system->injectMouseButtonUp(convertButton(id));
	}

	return true;
}

#include "utils.h"

using namespace Ogre;

// cube -size/2,-size/2,-size/2 -> size/2,size/2,size/2
MeshPtr createCubeMesh(SceneManager *sm, const std::string &name, const std::string &mat, Real size) {
	ManualObject *cube = sm->createManualObject(name);

	cube->begin(mat);
		// front side
		cube->position(-size/2,-size/2,-size/2);
		cube->position(size/2,-size/2,-size/2);
		cube->position(size/2,size/2,-size/2);
		cube->position(-size/2,size/2,-size/2);

		// back side
		cube->position(-size/2,-size/2,size/2);
		cube->position(size/2,-size/2,size/2);
		cube->position(size/2,size/2,size/2);
		cube->position(-size/2,size/2,size/2);

		cube->quad(3,2,1,0); // front
		cube->quad(4,5,6,7); // back
		cube->quad(4,7,3,0); // left
		cube->quad(6,5,1,2); // right
		cube->quad(7,6,2,3); // top
		cube->quad(0,1,5,4); // bottom
	cube->end();

	return cube->convertToMesh(name);
}

CEGUI::MouseButton convertButton(OIS::MouseButtonID id) {
	switch (id) {
		case OIS::MB_Left: return CEGUI::LeftButton;
				   break;
		case OIS::MB_Right: return CEGUI::RightButton;
				   break;
		case OIS::MB_Middle: return CEGUI::MiddleButton;
				   break;
		default: return CEGUI::LeftButton;
			 break;
	}
}

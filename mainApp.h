#ifndef MAINAPP_DEFINED
#define MAINAPP_DEFINED

#include <Ogre.h>
#include "game.h"

#define DEF_WIDTH	800
#define DEF_HEIGHT	600
#define DEF_FS		false

using namespace Ogre;

class MainApp : public WindowEventListener {
	private:
		Root *root;
		RenderSystem *rSys;
		RenderWindow *win;
		Game *game;

		bool createRenderSystem();
		void createRenderWindow(const std::string &title, int w, int h, bool fs);
		void createResources();
		
	public:
		MainApp();
		~MainApp();

		bool init(const std::string& title, int w = DEF_WIDTH, int h = DEF_HEIGHT, bool fs = DEF_FS);
		void destroy();

		void run();

		void windowResized(RenderWindow *rw);
		void windowClosed(RenderWindow *rw);
		void windowMoved(RenderWindow *rw) {};
		void windowFocusChange(RenderWindow *rw) {};

};

#endif

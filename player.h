#ifndef PLAYER_H_DEFINED
#define PLAYER_H_DEFINED

#include <Ogre.h>
#include <OIS.h>
#include <vector>

#include <btBulletDynamicsCommon.h>

const unsigned int WALL_QUERY_MASK = 0x00000001;
const unsigned int PLAYER_QUERY_MASK = 0x00000010;
const unsigned int BULLET_QUERY_MASK = 0x00000100;

#define MAX_BULLETS 50

using namespace Ogre;

typedef struct bullet {
	SceneNode *node;
	btRigidBody *body;
	unsigned int ttl;
} Bullet;

class Player {
	private:
		const Vector3 cube_size, bullet_size;

		SceneManager *sceneMgr;
		Entity *tmp_ent; //tmp debug

		unsigned int fired;
		std::vector<Bullet> bullets;
		AxisAlignedBoxSceneQuery *bullet_query;

		SceneNode *node;
		btRigidBody *body;
		SceneNode *bullet_node;
		float speed;
		float turn_speed;
		float angle;
		Vector3 aim_to;

		OIS::KeyCode key_up,key_down,key_left,key_right,key_fire;

		Bullet *getBulletByNode(SceneNode *node);

	public:
		Player();
		~Player();

		void init(SceneManager *sm, const std::string &name);

		void setKeys(OIS::KeyCode up, OIS::KeyCode down, OIS::KeyCode left, OIS::KeyCode right, OIS::KeyCode fire);
		OIS::KeyCode getKeyUp() {return key_up;};
		OIS::KeyCode getKeyDown() {return key_down;};
		OIS::KeyCode getKeyLeft() {return key_left;};
		OIS::KeyCode getKeyRight() {return key_right;};
		OIS::KeyCode getKeyFire() {return key_fire;};

		const float getSpeed() {return speed;};
		const float getTurnSpeed() {return turn_speed;};

		void setPos(Vector3 p);
		void setDir(Vector3 d);
		const Vector3& getPos();
		Vector3 getDir();

		void moveForward();
		void moveBack();
		void turnLeft();
		void turnRight();
		void fire(btDynamicsWorld *col_world);
		void aim(const Vector3 &p); // aim at point

		void preupdate(float dt);
		void postupdate(float dt);

		btRigidBody* getBody() {return body;};
};

#endif

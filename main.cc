#include "mainApp.h" 

int main(int argc, char ** argv) {
	MainApp app;
	if (!app.init("lTanks"))
		return 1;

	app.run();

	app.destroy();

	return 0;
}

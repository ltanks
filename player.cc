#include "player.h"
#include "utils.h"

Player::Player():cube_size(Vector3(5,5,5)),bullet_size(cube_size*0.3),fired(0),node(NULL),speed(0.0),angle(0.0) {
}

Player::~Player() {
}

void Player::init(SceneManager *sm, const std::string &name) {
	// bullet
	createCubeMesh(sm, name+"_mesh", "Red", 5);

	node = sm->getRootSceneNode()->createChildSceneNode(name);
	Entity *ent = sm->createEntity(name+"_ent",name+"_mesh");
//	ent->setQueryFlags(PLAYER_QUERY_MASK);
	node->attachObject(ent);

	tmp_ent = sm->createEntity(name+"_ent_bt_tmp",name+"_mesh");

	bullet_node = sm->getRootSceneNode()->createChildSceneNode(name+"_bt");

	sceneMgr = sm;

	// rigid body
	btCollisionShape *pl_shape = new btBoxShape(btVector3(2.5,2.5,2.5));
	btVector3 inertia(0,0,0);
	float mass = 1.0;
	pl_shape->calculateLocalInertia(mass, inertia);

	btTransform transform;
	transform.setIdentity();
	btDefaultMotionState *ms = new btDefaultMotionState(transform);
	btRigidBody::btRigidBodyConstructionInfo rb_info(mass, ms, pl_shape, inertia);
	body = new btRigidBody(rb_info);
//	body->setSleepingThresholds(0.0, 0.0);
	body->setActivationState(DISABLE_DEACTIVATION);	
	body->setAngularFactor(0.0);
}

void Player::setKeys(OIS::KeyCode up, OIS::KeyCode down, OIS::KeyCode left, OIS::KeyCode right, OIS::KeyCode fire) {
	key_up = up;
	key_down = down;
	key_left = left;
	key_right = right;
	key_fire = fire;
}

void Player::moveForward() {
//	if (speed < 1.0)
//		speed += 0.001;
	speed = 5.0;
}

void Player::moveBack() {
//	if (speed > -0.1)
//		speed -= 0.001;
	speed = -5.0;		
}

void Player::turnLeft() {
	turn_speed = -0.03;
}

void Player::turnRight() {
	turn_speed = 0.03;
}

void Player::fire(btDynamicsWorld *col_world) {
	if (fired < MAX_BULLETS) {
		Vector3 dir = aim_to - node->getWorldPosition();
		dir.y = 0.0;
		dir.normalise();
		Vector3 pos = node->getWorldPosition()+dir*(cube_size/2+bullet_size*2);

		Bullet b;
		btCollisionShape *shape = new btBoxShape(btVector3(2.5,2.5,2.5)*0.3);
		btVector3 inertia(0,0,0);
		shape->calculateLocalInertia(1.0, inertia);

		btTransform tran;
		tran.setIdentity();
		tran.setOrigin(btVector3(pos.x, pos.y, pos.z));
		btDefaultMotionState *ms = new btDefaultMotionState(tran);
		btRigidBody::btRigidBodyConstructionInfo rb_info(1.0, ms, shape, inertia);
		b.body = new btRigidBody(rb_info);
		b.body->setActivationState(DISABLE_DEACTIVATION);
		b.body->activate(true);
		b.body->setAngularFactor(0.0);
		col_world->addRigidBody(b.body);
		b.body->applyCentralImpulse(btVector3(dir.x,dir.y,dir.z)*50);
//		b.body->applyCentralForce(btVector3(0,0,2));

		b.node = bullet_node->createChildSceneNode();
		b.node->attachObject(tmp_ent);
		b.node->scale(0.3, 0.3, 0.3);
		b.node->setPosition(pos);
		b.node->setDirection(dir, Node::TS_WORLD);
		b.node->setVisible(true);

		b.ttl = 30;

		bullets.push_back(b);

		fired++;
	}
}

void Player::aim(const Vector3 &p) {
	aim_to = p;
}

void Player::setPos(Vector3 p) {
	btTransform tran;
	body->getMotionState()->getWorldTransform(tran);
	tran.setOrigin(btVector3(p.x, p.y, p.z));
	body->getMotionState()->setWorldTransform(tran);
	body->setCenterOfMassTransform(tran);

	node->setPosition(p);
}
void Player::setDir(Vector3 d) {
	//node->setDirection(d);
}

const Vector3& Player::getPos() {
	return node->getPosition();
}

Vector3 Player::getDir() {
	return -node->getOrientation().zAxis();
}

void Player::preupdate(float dt) {
}

void Player::postupdate(float dt) {
	angle += turn_speed*0.3;

	btTransform tran;
//	body->setAngularVelocity(btVector3(0,0,0));
	body->getMotionState()->getWorldTransform(tran);
	tran.setRotation(btQuaternion(btVector3(0.0,1.0,0.0),angle));
//	body->setAngularVelocity(btVector3(0,0,0));

	btVector3 fwd = tran.getBasis()[2];
	fwd.normalize();

	body->setLinearVelocity(fwd*speed);
	body->getMotionState()->setWorldTransform(tran);
	body->setCenterOfMassTransform(tran);

	btVector3 p = body->getCenterOfMassPosition();
//	btVector3 d = body->getOrientation() * btVector3(0,0,1);

	node->setPosition(p.x(),p.y(),p.z());
	node->setDirection(fwd[0],fwd[1],fwd[2], Node::TS_WORLD);

	for (std::vector<Bullet>::iterator i = bullets.begin(); i != bullets.end(); i++) {
		p = i->body->getCenterOfMassPosition();
		i->node->setPosition(p.x(), p.y(), p.z());
	}

	speed = 0.0;
	turn_speed = 0.0;
}

Bullet* Player::getBulletByNode(SceneNode *node) {
//	for (int i = 0; i < MAX_BULLETS; i++)
//		if (node == bullets[i].node)
//			return bullets+i;

	return NULL;
}

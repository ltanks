#ifndef GAME_H_DEFINED 
#define GAME_H_DEFINED

#include <Ogre.h>
#include <OIS.h>
#include <OgreNoMemoryMacros.h>
#include <CEGUI.h>
#include <OgreCEGUIRenderer.h>
#include <OgreMemoryMacros.h>
#include <btBulletDynamicsCommon.h>

#include "LinearMath/btAlignedObjectArray.h"

class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;
class btDynamicsWorld;

//#include <btBulletDynamicsCommon.h>

#include "player.h"

#define DATA_DIR "data/levels/"
#define MAP_SIZE 20
#define CUBE_SIZE 5

using namespace Ogre;

class Game : public OIS::KeyListener, public OIS::MouseListener {
	private:
		SceneManager *sceneMgr;
		Camera *cam;
		Viewport *vp;
		SceneNode *map_node;
		Player player;

		OverlayElement *cam_pos,*pl_pos,*pl_dir,*pl_speed;

		OIS::InputManager *input;
		OIS::Keyboard *keyboard;
		OIS::Mouse *mouse;

		CEGUI::OgreCEGUIRenderer *gui_render;
		CEGUI::System *gui_system;
		CEGUI::Window *gui_main_menu, *gui_single_menu, *gui_multi_menu, *gui_setup_menu, *gui_credits_menu, *gui_player_menu, *gui_graphics_menu, *gui_sound_menu;
		bool menu_visible;

		bool running;
		int map[MAP_SIZE][MAP_SIZE];

		btDefaultCollisionConfiguration *col_conf;
		btCollisionDispatcher *col_dispatch;
		btBroadphaseInterface *col_pair_cache;
		btConstraintSolver *col_solver;
		btDynamicsWorld *col_world;
		btAlignedObjectArray<btCollisionShape *> col_shapes;

		void initPhysics();

		bool menuOnClick(const CEGUI::EventArgs &args);

		void initInput(RenderWindow *win, bool buf);
		void initCamera();
		void initViewport(RenderWindow *win);
		void initGUI(RenderWindow *win);
		void setMouseArea(int w, int h);

		bool loadLevel(const std::string &name);

	public:
		Game();
		~Game();

		void init(Root *root, RenderWindow *win, bool buf = true);

		void capture();
		void update(float dt);

		void quit();
		bool isRunning() {return running;};

		void setWinSize(int w, int h);

		bool keyPressed(const OIS::KeyEvent &e);
		bool keyReleased(const OIS::KeyEvent &e);

		bool mouseMoved(const OIS::MouseEvent &e);
		bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);
};

#endif

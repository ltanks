#ifndef UTILS_H_DEFINED
#define UTILS_H_DEFINED

#include <Ogre.h>
#include <OIS.h>
#include <OgreNoMemoryMacros.h>
#include <CEGUI.h>
#include <OgreMemoryMacros.h>

Ogre::MeshPtr createCubeMesh(Ogre::SceneManager *sm, const std::string &name, const std::string &mat, Ogre::Real size = 1.0f);
CEGUI::MouseButton convertButton(OIS::MouseButtonID id);

#endif

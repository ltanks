CXX=g++
CXXFLAGS=-Wall -Wno-deprecated `pkg-config --cflags OGRE OIS CEGUI CEGUI-OGRE bullet`
LIBS=`pkg-config --libs OGRE OIS CEGUI CEGUI-OGRE bullet`
COMP=$(CXX) $(CXXFLAGS)
EXE=ltanks
OBJS=mainApp.o game.o utils.o player.o 

.PHONY: clean
all: $(OBJS) main.o
	$(CXX) main.o $(OBJS) $(LIBS) -o $(EXE)

main.o: main.cc

%.o: %.cc %.h
	$(COMP) -c -o $@ $<
	
clean:
	rm -f *.o *.log $(EXE)
